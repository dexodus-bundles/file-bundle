<?php

declare(strict_types=1);

namespace Dexodus\FileBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FileBundle extends Bundle
{
}
